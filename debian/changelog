pyaimt (0.8.0.1-5) UNRELEASED; urgency=medium

  [ Michal Čihař ]
  * NOT RELEASED YET
  * Bump standards to 3.9.6.

  [ Ondřej Nový ]
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Remove trailing whitespaces
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:12:55 -0500

pyaimt (0.8.0.1-4) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Michal Čihař ]
  * Bump standards to 3.9.5.
  * Switch to debhelper 9.
  * Fix watch file.

 -- Michal Čihař <nijel@debian.org>  Mon, 11 Aug 2014 13:18:29 +0200

pyaimt (0.8.0.1-3) unstable; urgency=low

  * Fix watch file to work after recent changes on code.google.com.
  * Fix permissions of /var/lib/pyaimt (Closes: #672291).
  * Remove extra file in clean (Closes: #671549).
  * Bump standards to 3.9.3.
  * Migrate to dh_python2.
  * Init script now understands status command.

 -- Michal Čihař <nijel@debian.org>  Thu, 10 May 2012 09:22:33 +0200

pyaimt (0.8.0.1-2) unstable; urgency=low

  * Adjust provided service name in init script.
  * Fix naming of init script in source package.
  * Init script should depend on $remote_fs.
  * No need to build depend on python-dev, python is enough.
  * Convert to 3.0 (quilt) source format.

 -- Michal Čihař <nijel@debian.org>  Sat, 02 Jan 2010 13:06:52 +0100

pyaimt (0.8.0.1-1) unstable; urgency=low

  * New upstream version.
  * Bump policy to 3.8.3.
  * Drop patches which seem not to be needed (pyicq20070823.patch and
    fixups.patch).

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Aug 2009 22:21:10 +0200

pyaimt (0.8a-9) unstable; urgency=low

  * Do not ship /var/run/pyaimt in package.
  * Update to policy 3.8.1.
  * Switch to debhelper 7 and python-support.
  * Update debian/copyright.
  * Switch from dpatch to quilt.

 -- Michal Čihař <nijel@debian.org>  Thu, 07 May 2009 14:43:17 +0200

pyaimt (0.8a-8) unstable; urgency=low

  [ Michal Čihař ]
  * Add missing debian/README.source.
  * Update standards to 3.8.0.
  * Document patches.
  * Do not crash when connection is lost (Closes: #504393).

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Michal Čihař <nijel@debian.org>  Wed, 05 Nov 2008 13:38:40 +0100

pyaimt (0.8a-7) unstable; urgency=low

  * Reimplement missing removePID (Closes: #489308).

 -- Michal Čihař <nijel@debian.org>  Sun, 06 Jul 2008 23:24:52 +0200

pyaimt (0.8a-6) unstable; urgency=low

  * Move packaging to Python Applications Packaging Team:
    - Change Vcs fields in debian/control.
    - Add team to Uploaders.
  * Fix names of suggested jabber daemons.

 -- Michal Čihař <nijel@debian.org>  Tue, 27 May 2008 14:19:43 +0200

pyaimt (0.8a-5) unstable; urgency=low

  * Report startup failures (Closes: #476096).
  * Use LSB logging functions in init script.

 -- Michal Čihař <nijel@debian.org>  Mon, 14 Apr 2008 15:55:49 +0200

pyaimt (0.8a-4) unstable; urgency=low

  * Adjusted Vcs-* headers to point to trunk.
  * Cleanup dependencies, add missing python-openssl.
  * Remove pyaimt user and group on purge.

 -- Michal Čihař <nijel@debian.org>  Mon, 03 Mar 2008 17:58:26 +0100

pyaimt (0.8a-3) unstable; urgency=low

  * Update home page references as upstream has moved to
    http://code.google.com/p/pyaimt/.

 -- Michal Čihař <nijel@debian.org>  Mon, 17 Dec 2007 09:51:40 +0900

pyaimt (0.8a-2) unstable; urgency=low

  * Replace ICQ by AIM in description.

 -- Michal Čihař <nijel@debian.org>  Wed, 12 Dec 2007 17:18:29 +0900

pyaimt (0.8a-1) unstable; urgency=low

  * Initial upload to Debian (Closes: #406656).

 -- Michal Čihař <nijel@debian.org>  Wed, 12 Dec 2007 16:45:27 +0900

pyaimt (0.8a-0.nijel.2) unstable; urgency=low

  * Fix logging in getvCardNotInList.

 -- Michal Čihař <nijel@debian.org>  Wed, 12 Dec 2007 16:20:19 +0900

pyaimt (0.8a-0.nijel.1) unstable; urgency=low

  * Fixed exception with LegacyConnection instance has no attribute
    'getvCardNotInList'.

 -- Michal Čihař <nijel@debian.org>  Mon, 26 Nov 2007 12:07:50 +0900

pyaimt (0.8a-0.nijel.0) unstable; urgency=low

  * Just package also this beast.

 -- Michal Čihař <nijel@debian.org>  Wed, 10 Oct 2007 09:36:56 +0900
